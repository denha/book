app.controller('LoginController', function ($scope, Auth, $location) {
    $scope.login = function () {
        Auth.login($scope.username, $scope.password).then(function (response) {
            $location.url('/')
        })
    }
});

app.controller('RegisterController', function ($scope, $location, Auth) {
    $scope.register = function() {
        Auth.register($scope.username, $scope.password).then(function (response) {
            // $location.url('/')
        });
    }
});