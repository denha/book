app.factory('Auth', function ($http, $q, $location) {
    var currentUser = {};
    $http.get('/user/loggedin').then(function (response) {
        currentUser = response.data;
        // console.log(currentUser);
    }, function (error) {
        console.log(error);
    });
    return {
        login: function (username, password) {
            return $http.post('/user/login', {username: username, password: password}).then(function(response) {
                console.log(response);
                currentUser = response.data;
            })
        },
        isLoggedIn: function () {
            return currentUser.hasOwnProperty('username')
        },
        getCurrentUser: function () {
            return currentUser;
        },
        logout: function () {
            currentUser = {};
            return $http.post('/user/logout');
        },
        register: function(username, password) {
            return $http.post('/user/register', {username: username, password: password}).then(function (response) {
                currentUser = response.data;
                console.log(response.data);
            })
        },
        update: function(properties) {
            return $http.post('/user/update', properties).then(function (response) {
                currentUser = response.data;
                console.log(response.data);
            })
        }
    }
});