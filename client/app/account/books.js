app.controller('BooksController', function ($scope, $http, Book, Auth) {
    $scope.searching = false;
    $scope.search = function (title) {
        $scope.searching = true;
        $http.jsonp(`https://www.googleapis.com/books/v1/volumes?q=intitle:${title}&projection=lite&callback=JSON_CALLBACK`).then(function (response) {
            console.log(response);
            $scope.searchResults = response.data.items;
            $scope.searching = false;
        });
    };

    $scope.user = Auth.getCurrentUser();

    $scope.$watch('user', function () {
        ownedBooks();

        Book.find({params: {requester: $scope.user._id}}).then(function (response) {
            $scope.trades = response.data;
        });
    });
    
    function ownedBooks() {
        Book.find({params: {username: $scope.user.username, populate: true}}).then(function (response) {
            console.log(response.data);
            $scope.books = response.data;
        })
    }

    $scope.create = function (book) {
        Book.create(book).then(function () {
            ownedBooks();
        })
    };

    $scope.delete = function (book) {
        Book.delete(book).then(function () {
            ownedBooks();
        })
    };

    $scope.accept = function (book) {
        Book.update(book, {accepted: true}).then(function (response) {
            console.log(response)
            ownedBooks();
        })
    }
});


app.directive('isAccepted', function() {
    return {
        restrict: 'E',
        controller: function($scope) {
            if ($scope.book.accepted) {
                $scope.message = 'Accepted!';
                $scope.style = 'color: green'
            } else {
                $scope.message = 'Not accepted yet.';
                $scope.style = 'color: red;'
            }
        },
        template: '<p style="{{style}}">{{message}}</p>'
    }
})