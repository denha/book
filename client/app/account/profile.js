app.controller('ProfileController', function ($scope, $http, Auth) {
    $scope.$watch(Auth.getCurrentUser, function(newValue) {
        $scope.user = newValue;
    })

    $scope.save = function() {
        var props = {};
        if ($scope.user.hasOwnProperty('name')) {
            props.name = $scope.user.name
        }
        if ($scope.user.hasOwnProperty('city')) {
            props.city = $scope.user.city
        }
        console.log(props);
        Auth.update(props)
    }
});