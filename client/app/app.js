'use strict';

var app = angular.module('angularApp', ['ngRoute', 'ui.bootstrap']);

app.config(function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider.when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController'
    });
    //
    // $routeProvider.when('/add', {
    //     templateUrl: 'app/main/search.html',
    //     controller: 'SearchController'
    // });
    $routeProvider.when('/login', {
        templateUrl: 'app/account/login.html',
        controller: 'LoginController'
    });
    $routeProvider.when('/logout', {
        template: '',
        controller: function ($location, Auth) {
            Auth.logout().then(function () {
                $location.url('/');
            });
        }
    });
    $routeProvider.when('/register', {
        templateUrl: 'app/account/register.html',
        controller: 'RegisterController'
    });
    
    $routeProvider.when('/profile', {
        templateUrl: 'app/account/profile.html',
        controller: 'ProfileController'
    });
    $routeProvider.when('/profile/books', {
        templateUrl: 'app/account/books.html',
        controller: 'BooksController'
    });
    
    $routeProvider.otherwise({
        redirectTo: '/'
    });
    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push(function ($q, $location) {
        return {
            response: function (response) {
                return response
            },
            responseError: function (response) {
                if (response.status === 401)
                    $location.url('/login');
                return $q.reject(response)
            }
        }
    })
});
