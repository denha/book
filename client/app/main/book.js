app.factory('Book', function ($http, Auth) {

    
    return {
        create: function (book) {
            return $http.post('/api/books', book).then(function (response) {
                console.log(response);
            })
        },
        index: function () {
            return $http.get('/api/books');
        },
        find: function (query) {
            return $http.get('/api/books', query);
        },
        // mine: function () {
        //     var username = Auth.getCurrentUser().username;
        //     if (username) {
        //         return $http.get('/api/books', {params: {username: username, populate: true}});
        //     } else {
        //         return Promise.reject();
        //     }
        // },
        delete: function (book) {
            return $http.delete('/api/books/' + book._id).then(function (response) {
                console.log(response);
            })
        },
        update: function (book, updates) {
            // The parameters to update and their values. eg) {trade: 'some user'}
            return $http.put('/api/books/' + book._id, updates).then(function(response) {
                console.log(response);
            })
        }
    }
});