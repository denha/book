app.controller('MainController', function ($scope, $http, Book, Auth) {
    $scope.pageSize = 5;
    $scope.page = 1;

    Book.index().then(function (response) {
        $scope.bookObjects = response.data;
        $scope.books = [];
        for (var i = 0; i < $scope.bookObjects.length; i++) {
            $http.jsonp(`${$scope.bookObjects[i].selfLink}?callback=JSON_CALLBACK`).then(function (response) {
                $scope.books[this.i] = response.data;
            }.bind({i: i}));
        }
        $scope.$watch('page', function () {
            var begin = (($scope.page - 1) * $scope.pageSize),
                end = begin + $scope.pageSize;
            $scope.filteredBooks = $scope.books.slice(begin, end);
        });
        $scope.$watchCollection('books', function () {
            var begin = (($scope.page - 1) * $scope.pageSize),
                end = begin + $scope.pageSize;
            $scope.filteredBooks = $scope.books.slice(begin, end);
        });
        Book.find({params: {username: Auth.getCurrentUser().username, populate: true}}).then(function (response) {
            $scope.mybooks = response.data;
        });
    });

    $scope.trade = function (book, selectedBook) {
        console.log(book)
        console.log(selectedBook)
        if (selectedBook)
            Book.update(book, {requester: Auth.getCurrentUser()._id, book: selectedBook, accepted: false})
    }
});
