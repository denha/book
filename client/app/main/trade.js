// app.factory('Trade', function ($http, Auth) {
//     return {
//         create: function (trade) {
//             return $http.post('/api/trades', trade).then(function (response) {
//                 console.log(response);
//             })
//         },
//         index: function () {
//             return $http.get('/api/trades');
//         },
//         mine: function () {
//             return $http.get('/api/trades', {params:{username: Auth.getCurrentUser().username}});
//         },
//         delete: function (book) {
//             return $http.delete('/api/trades/' + book._id).then(function (response) {
//                 console.log(response);
//             })
//         },
//         update: function (book, updates) {
//             // The parameters to update and their values. eg) {trade: 'some user'}
//             return $http.put('/api/trades/' + book._id, updates).then(function(response) {
//                 console.log(response);
//             })
//         }
//     }
// });