app.directive('navbar', function () {
    return {
        templateUrl: 'app/navbar/navbar.html',
        restrict: 'E',
        controller: 'NavbarController'
    };
});

app.controller('NavbarController', function ($scope, Auth) {
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.getCurrentUser = Auth.getCurrentUser
});