'use strict';

import gulp from 'gulp';
import {stream as wiredep} from 'wiredep';
import del from 'del';
import runSequence from 'run-sequence';
var lib = require('bower-files')();
var series = require('stream-series');
var ForkEvents = require('fork-events');

import gulpLoadPlugins from 'gulp-load-plugins';
var $ = gulpLoadPlugins();

var browserSync = require('browser-sync').create();

var path = {};
path.build = 'build';
path.dist = 'dist';
path.client = 'client';
path.server = 'server';
path.buildClient = `${path.build}/${path.client}`;
path.buildServer = `${path.build}/${path.server}`;
path.serverjs = `${path.buildServer}/server.js`;

gulp.task('default', ['browser-sync']);

var server
gulp.task('browser-sync', ['client:inject'], function () {
    server = ForkEvents.fork('build/server/server.js');
    // 'started' event is emitted from the server when it starts listening
    server.on('started', function(e) {
        if (e.reforked) {
            browserSync.reload({ stream: false });
        } else {
            browserSync.init({
                proxy: 'http://localhost:9000',
                notify: false
            });
        }
    });

    gulp.watch('server/**/*.js', function() {
        runSequence('server', 'reload');
    });
    gulp.watch(`./${path.client}/app/**/*.js`, function() {
        runSequence('scripts', 'browserreload');
    });
    gulp.watch(`./${path.client}/**/*.scss`,function() {
        runSequence('styles', 'browserreload');
    });
    gulp.watch(`./${path.client}/**/*.html`, function() {
        runSequence('client:inject', 'browserreload');
    });
    gulp.watch('bower.json', ['bower']);
});

gulp.task('reload', function () {
    server.refork();
});


gulp.task('browserreload', function () {
    browserSync.reload();
});

gulp.task('client:inject', ['bower'], function () {
    return gulp.src(`${path.buildClient}/index.html`)
        .pipe($.inject(gulp.src([`${path.buildClient}/app/**/*.js`, `${path.buildClient}/app/**/*.css`], {read: false}), {relative: true}))
        .pipe(gulp.dest(`${path.buildClient}`));
});

gulp.task('bower', ['wiredep'], function () {
    return gulp.src(`${path.client}/bower_components/**/*.*`).pipe(gulp.dest(`${path.buildClient}/bower_components`));
});

// inject bower components
gulp.task('wiredep', ['watch'], function () {
    return gulp.src(`./${path.client}/index.html`)
        .pipe(wiredep({
            directory: './client/bower_components',
            ignorePath: '..'
        }))
        .pipe(gulp.dest(`./${path.buildClient}`));
});

gulp.task('watch', ['server', 'scripts', 'styles', 'html'], function () {
    // gulp.watch('server/**/*.js', ['babel-server']);
    // gulp.watch(`./${path.client}/app/**/*.js`, ['scripts']);
    // gulp.watch(`./${path.client}/**/*.scss`, ['styles']);
    // gulp.watch(`./${path.client}/**/*.html`, ['html']);
    // gulp.watch('bower.json', ['bower']);
});

gulp.task('scripts', function () {
    return gulp.src('client/app/**/*.js', {base: 'app'})
        .pipe($.sourcemaps.init())
        .pipe($.babel())
        .pipe($.ngAnnotate())
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(path.buildClient));
});

gulp.task('server', function () {
    return gulp.src('server/**/*.js')
        .pipe($.sourcemaps.init())
        .pipe($.babel())
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(path.buildServer));
});

gulp.task('styles', () => {
    return gulp.src(`${path.client}/**/*.scss`)
        .pipe($.sourcemaps.init())
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.autoprefixer(), {browsers: ['last 1 version']})
        // .pipe($.sourcemaps.write(), '.')
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(path.buildClient));
});

gulp.task('html', function () {
    gulp.src(`${path.client}/**/*.html`)
        // .pipe($.jade())
        .pipe(gulp.dest(path.buildClient));
});

gulp.task('clean', function () {
    del([`${path.build}/!(.git*|.openshift|Procfile)**`])
});

gulp.task('clean:dist', function () {
    del([`${path.dist}/!(.git*|.openshift|Procfile)**`])
});

// Production
gulp.task('dist', function (done) {
    runSequence('clean:dist', 'client:inject', 'distTasks', 'dist:browser-sync', function () {
        done();
    })
});

gulp.task('distTasks', ['dist:inject', 'dist:server', 'copy']);

gulp.task('dist:scripts:app', function () {
    return gulp.src(path.buildClient + '/app/**/*.js')
        .pipe($.concat('app.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('dist/client'));
});

gulp.task('dist:scripts:vendor', function () {
    gulp.src(lib.ext('js').files)
        .pipe($.concat('vendor.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('dist/client'));
});

gulp.task('dist:html', function () {
    return gulp.src(path.client + '/**/*.html')
        .pipe(gulp.dest('dist/client'))
});

gulp.task('dist:styles', function () {
    return gulp.src(path.buildClient + '/app/**/*.css')
        .pipe(gulp.dest('dist/client'))
});

gulp.task('dist:inject', ['dist:scripts:vendor', 'dist:scripts:app', 'dist:styles', 'dist:html'], function () {
    var vendorStream = gulp.src(['dist/client/vendor.js'], {read: false});
    var appStream = gulp.src(['dist/client/app.js', 'dist/client/**/*.css'], {read: false});
    return gulp.src('dist/client/index.html')
        .pipe($.inject(series(vendorStream, appStream), {relative: true}))
        .pipe(gulp.dest('dist/client'));
});

gulp.task('dist:server', function () {
    return gulp.src('build/server/**/*')
        .pipe(gulp.dest('dist/server'));
});

gulp.task('copy', () => {
    return gulp.src([
            'package.json',
            'bower.json',
            '.bowerrc'
        ], {cwdbase: true})
        .pipe(gulp.dest(path.dist));
});

gulp.task('dist:browser-sync', ['dist:nodemon'], function () {
    return browserSync.init(null, {
        proxy: 'http://localhost:9000',
        files: 'dist/**/*.*'
    });
});

gulp.task('dist:nodemon', function (cb) {
    var started = false;
    return $.nodemon({
        script: path.dist + '/server/server.js',
        env: {'NODE_ENV': process.env.NODE_ENV || 'development'},
        watch: path.buildServer
    }).on('start', function () {
        if (!started) {
            cb();
            started = true;
        }
    }).on('log', function (log) {
        console.log('[nodemon]' + log.message);
    });
});