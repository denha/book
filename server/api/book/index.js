import http from 'http';
import request from 'request';

var express = require('express');
var router = express.Router();
var Book = require('./model').Model;

router.get('/', index);

// router.get('/:id', show);
router.post('/', create);
router.put('/:id', update);
// router.patch('/:id', update);
router.delete('/:id', destroy);

function index(req, res) {
    console.log(req.query);
    var query;
    if (req.query.populate) {
        delete req.query.populate;
        query = Book.find(req.query);
        query.populate('requester', '-password');
        query.populate('book');
    } else {
        query = Book.find(req.query);
    }
    console.log(2);
    console.log(req.query);
    if (req.query.skip && req.query.limit) {
        return query.skip(Number(req.query.skip)).limit(Number(req.query.limit))
            .then(respondWithResult(res))
            .catch(handleError(res));
    }
    return query.exec()
        .then(respondWithResult(res))
        .catch(handleError(res));
}

function update(req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Book.findById(req.params.id).exec()
        .then(handleEntityNotFound(res))
        .then(saveUpdates(req.body))
        .then(respondWithResult(res))
        .catch(handleError(res));
}

function saveUpdates(updates) {
    console.log(updates);
    return function(entity) {
        for (var attrname in updates) {
            entity[attrname] = updates[attrname];
        }
        return entity.save()
    };
}


function create(req, res) {
    var book = req.body;
    var object = {
        username: req.user.username,
        title: book.volumeInfo.title,
        selfLink: book.selfLink
        // , image: book.volumeInfo.imageLinks.thumbnail
    };
    console.log(object);
    return Book.create(object)
        .then(respondWithResult(res, 201))
        .catch(handleError(res));
}

function destroy(req, res) {
    return Book.findById(req.params.id).exec()
        .then(handleEntityNotFound(res))
        .then(removeEntity(res))
        .catch(handleError(res));
}

function removeEntity(res) {
    return function(entity) {
        if (entity) {
            return entity.remove()
                .then(() => {
                    res.status(204).end();
                });
        }
    };
}

function respondWithResult(res, statusCode) {
    statusCode = statusCode || 200;
    return function(entity) {
        if (entity) {
            res.status(statusCode).json(entity);
        }
    };
}

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function(err) {
        res.status(statusCode).send(err);
    };
}

function handleEntityNotFound(res) {
    return function(entity) {
        if (!entity) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
}

module.exports = router;