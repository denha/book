var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Book = new Schema({
    // title: {
    //     type: String,
    //     required: true
    // },
    username: {
        type: String,
        required: true
    },
    selfLink: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    // TODO use an array so multiple people can make trade requests for the same book.
    requester: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    // The book to swap for
    book: {
        type: Schema.Types.ObjectId,
        ref: 'Book'
    },
    accepted: Boolean
});
// Book.index({title: 1, username: -1}, {unique: true});

module.exports.Model = mongoose.model('Book', Book);
module.exports.Schema = Book;