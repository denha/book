import http from 'http';
import request from 'request';
var passport = require('passport');

var User = require('./model');
var LocalStrategy = require('passport-local').Strategy;
var express = require('express');
var router = express.Router();

var authenticated = function (req, res, next) {
    if (!req.isAuthenticated())
        res.send(401);
    else
        next();
};

passport.use(new LocalStrategy(function (username, password, done) {
    User.findOne({
        username: username
    }, function (err, user) {
        console.log(user);
        if (user !== null && username === user.username && password === user.password) {
            done(null, user);
        }
        else {
            done(null, false);
        }
    });
}));

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

// route to test if the user is logged in or not
router.get('/loggedin', function (req, res) {
    console.log(req.user);
    if (req.isAuthenticated()) {
        res.send(req.user);
    } else {
        res.end();
    }
    // res.send(req.isAuthenticated() ? req.user : null);
});
// route to log in
router.post('/login', passport.authenticate('local'), function (req, res, next) {
    res.send(req.user);
    // passport.authenticate('local', function (err, user, info) {
    //     console.log(req.body);
    //     if (err) return next(err);
    //     if (!user)
    //         return res.send('no');
    //     // return res.redirect('/login');
    //     req.login(user, function (err) {
    //         if (err) return next(err);
    //         res.send(req.user);
    //     });
    // })(req, res, next);
});
// route to log out
router.post('/logout', function (req, res) {
    req.logOut();
    res.sendStatus(200);
});
// https://vickev.com/#!/article/authentication-in-single-page-applications-node-js-passportjs-angularjs

router.post('/register', function (req, res, next) {
    if (!req.body || !req.body.username || !req.body.password) {
        res.sendStatus(400);
    } else {
        var user = new User({
            username: req.body.username,
            password: req.body.password
        });
        user.save(function (err) {
            if (err) {
                res.sendStatus(409);
            } else {
                req.login(user, function(err) {
                    if (err) {
                        console.log(err);
                    }
                    res.send(user);
                    // res.redirect('/register');
                });
            }
        })
    }
});

router.post('/update', authenticated, function (req, res) {
    console.log(req.body);
    User.findOne({username: req.user.username}, function (err, doc) {
        if (err) {
            res.send(err);
            return
        }
        if (req.body.name) {
            doc.name = req.body.name;
        }
        if (req.body.city) {
            doc.city = req.body.city;
        }
        doc.save();
        res.send(doc);
    });
});

module.exports = router;