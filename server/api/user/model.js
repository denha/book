var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: false
    }
});

// Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', User);
module.exports.Schema = User;