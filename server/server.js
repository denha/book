#!/usr/bin/env node

import express from 'express';
import path from 'path';
import http from 'http';
import mongoose from 'mongoose';
import passport from 'passport'

var app = express();

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var session = require('express-session');
var config = require('./config');
var MongoStore = require('connect-mongo')(session);

//Move up one directory from build/server/
app.use('/', express.static(__dirname + '/../client'));
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser());
app.use(session({
    store: new MongoStore({url:config.mongo.uri}),
    secret: config.secrets.session,
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/api/books', require('./api/book'));
app.use('/user', require('./api/user'));

app.route('/*').get((req, res) => {
    res.sendFile(path.resolve(__dirname + '/../client/index.html'));
});

var server = http.createServer(app);

mongoose.connect(config.mongo.uri, {db: {safe: true}});
mongoose.connection.on('error', function (err) {
    console.error('MongoDB connection error: ' + err);
    process.exit(-1);
});

var port = process.env.PORT || 9000;
server.listen(port, function () {
    console.log(`started on port ${port}`);
    if (process.send)
        process.send({event: 'started'});
});
